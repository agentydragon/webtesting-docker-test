# Bazel webtesting within Docker

```bash
docker build -t bazel-webtest-testrunner .
docker run -it \
  --mount type=bind,source=$(pwd),target=/repo \
  bazel-webtest-testrunner
```

In the container:

```bash
cd /repo
bazel test //...
```

