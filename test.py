import unittest
from testing.web import webtest


class BrowserTest(unittest.TestCase):
    def setUp(self):
        self.driver = webtest.new_webdriver_session()

    def test_simple(self):
        self.driver.get("http://www.python.org")
        assert "Python" in self.driver.title

    def tearDown(self):
        try:
            self.driver.quit()
        finally:
            self.driver = None

    # Your tests here


if __name__ == "__main__":
    unittest.main()
