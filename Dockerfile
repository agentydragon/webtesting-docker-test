FROM ubuntu:latest

# Add Bazel source.
RUN apt update && \
  apt install -y \
    curl \
    gnupg2
RUN curl https://bazel.build/bazel-release.pub.gpg | apt-key add -
RUN echo "deb [arch=amd64] https://storage.googleapis.com/bazel-apt stable jdk1.8" > /etc/apt/sources.list.d/bazel.list
RUN \
  apt remove -y \
    curl \
    gnupg2

# Install build dependencies and Bazel.
RUN apt update && \
  apt full-upgrade -y && \
  DEBIAN_FRONTEND=noninteractive apt install -y \
    bazel \
    git \
    python3 \
    python-is-python3 \
    python3-pip

# urllib3 seems to be required by Webdriver and not included:
# 2020/12/19 00:26:02 launching HTTP server at: 94d4d5cced37:43027
# 2020/12/19 00:26:02.463374 Listening on :35507
# Traceback (most recent call last):
#   File "execroot/__main__/bazel-out/k8-fastbuild/bin/test_chromium-local.sh.runfiles/__main__/test.py", line 2, in <module>
#     from testing.web import webtest
#   File "execroot/__main__/bazel-out/k8-fastbuild/bin/test_chromium-local.sh.runfiles/io_bazel_rules_webtesting/testing/web/webtest.py", line 27, in <module>
#     from selenium.webdriver.remote import remote_connection
#   File "execroot/__main__/bazel-out/k8-fastbuild/bin/test_chromium-local.sh.runfiles/org_seleniumhq_py/selenium/webdriver/__init__.py", line 18, in <module>
#     from .firefox.webdriver import WebDriver as Firefox  # noqa
#   File "execroot/__main__/bazel-out/k8-fastbuild/bin/test_chromium-local.sh.runfiles/org_seleniumhq_py/selenium/webdriver/firefox/webdriver.py", line 29, in <module>
#     from selenium.webdriver.remote.webdriver import WebDriver as RemoteWebDriver
#   File "execroot/__main__/bazel-out/k8-fastbuild/bin/test_chromium-local.sh.runfiles/org_seleniumhq_py/selenium/webdriver/remote/webdriver.py", line 27, in <module>
#     from .remote_connection import RemoteConnection
#   File "execroot/__main__/bazel-out/k8-fastbuild/bin/test_chromium-local.sh.runfiles/org_seleniumhq_py/selenium/webdriver/remote/remote_connection.py", line 24, in <module>
#     import urllib3
# ModuleNotFoundError: No module named 'urllib3'
RUN pip install urllib3
RUN pip3 install urllib3

# libglib, libnss3: needed for Chromedriver
# io_bazel_rules_webtesting/third_party/chromedriver/chromedriver.out/chromedriver_linux64/chromedriver: error while loading shared libraries: libglib-2.0.so.0: cannot open shared object file: No such file or directory
# io_bazel_rules_webtesting/third_party/chromedriver/chromedriver.out/chromedriver_linux64/chromedriver: error while loading shared libraries: libnss3.so: cannot open shared object file: No such file or directory
RUN DEBIAN_FRONTEND=noninteractive apt install -y libglib2.0-0 libnss3
RUN apt clean && apt autoremove -y
